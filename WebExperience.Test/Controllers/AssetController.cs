﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebExperience.Test.Models;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;

namespace WebExperience.Test.Controllers
{
    public class AssetController : ApiController
    {
        // TODO:
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework
        const int columns   = 7;
        const int aidCol    = 0;
        const int fnCol     = 1;
        const int mTypeCol  = 2;
        const int creaCol   = 3;
        const int emailCol  = 4;
        const int ctryCol   = 5;
        const int descCol   = 6;

        private bool isAssetIdFormatOk(string assetId){
            return (Regex.IsMatch(assetId, @"[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}", RegexOptions.IgnoreCase));
        }
        /* this method checks the  */
        private int checkParameters(string assetId, string	fileName, string mimeType, string createdBy, string	email, string country, string description)
        {
            /* assetId format is HEX(8)-HEX(4)-HEX(4)-HEX(4)-HEX(12) */
            if (!isAssetIdFormatOk(assetId)) return -1;
            // TODO : check the other fields
            return 0;
        }
        private string getDataPath()
        {
            string projectName = @"WebExperience.Test\";
            // process Path Parts ex: C:\Users\m18xUser\Documents\BitBucketTest\GeneralKnowledge.Test etc.
            string processPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            int stop = processPath.IndexOf(@"GeneralKnowledge.Test\") + projectName.Length; // get project name
            return (processPath.Substring(0, stop) + @"App_Data\");
        }

        public Asset[] Get()
        {
            List <int> errorLines = new List<int>();
            List <Asset> allAssets = new List<Asset>();
            try
            {
                string[] allRecords = File.ReadAllLines(getDataPath() + "AssetImport.csv");
                /* the loop starts at 1 because 0 is the header line */
                for(int iRec = 1; iRec < allRecords.Length; iRec++){
                    string[] crntRecord = allRecords[iRec].Split(',');
                    if (crntRecord.Length < columns) // if not enough columns to match an asset
                    {
                        // must be an error, continue
                        errorLines.Add(iRec);
                        continue;
                    }
                    switch(checkParameters(crntRecord[aidCol], crntRecord[fnCol], crntRecord[mTypeCol], crntRecord[creaCol], crntRecord[emailCol], crntRecord[ctryCol], crntRecord[descCol]))
                    {
                        case (0): // everything is OK
                            Console.WriteLine("Adding new line");
                            allAssets.Add(new Asset(crntRecord[aidCol], crntRecord[fnCol], crntRecord[mTypeCol], crntRecord[creaCol], crntRecord[emailCol], crntRecord[ctryCol], crntRecord[descCol]));
                            break;
                        default:
                            errorLines.Add(iRec);
                            // TODO behaviour for other possible errors.
                            break;
                    } 
                }
            }
            catch (Exception e) {
                Console.WriteLine("AN EXCEPTION OCCURED : {0}", e.ToString());
            }

            return (allAssets.ToArray());
        }

    }
}
