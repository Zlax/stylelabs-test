﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        /* rescaling function from http://stackoverflow.com/questions/1922040/resize-an-image-c-sharp */
        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode    = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode  = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode      = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode    = PixelOffsetMode.HighQuality;

                using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        /* inspired from http://stackoverflow.com/questions/10077219/unable-to-locate-fromstream-in-image-class */
        private static Image GetImageFromUrl(string url)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);

            using (HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                using (Stream stream = httpWebReponse.GetResponseStream())
                {
                    return Image.FromStream(stream);
                }
            }
        }

        private string getResourcePath()
        {
            string projectName = @"GeneralKnowledge.Test\";
            // process Path Parts ex: C:\Users\m18xUser\Documents\BitBucketTest\GeneralKnowledge.Test etc.
            string processPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            int stop = processPath.IndexOf(@"GeneralKnowledge.Test\") + projectName.Length; // get project name
            return (processPath.Substring(0, stop) + @"Resources\");
        }

        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)
            try
            {
                Image original = GetImageFromUrl(@"http://3.bp.blogspot.com/-6M1VnBAAHw4/UA1SZ4LdQOI/AAAAAAAABpQ/X6yzd2QiGHE/s1600/bmw-sport-car-wallpaper.jpg");
                string resourcePath = getResourcePath();
                
                /* 3 steps :
                 * 1) resize the original image
                 * 2) save the resized image
                 * 3) print the name and path of the saved image */
                Bitmap preview = ResizeImage(original, 1200, 1600);
                preview.Save(resourcePath + "preview.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                Console.WriteLine("Image preview.jpeg saved in "+ resourcePath);

                Bitmap thumbnail = ResizeImage(original, 80, 120);
                thumbnail.Save(resourcePath + "thumbnail.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                Console.WriteLine("Image thumbnail.jpeg saved in " + resourcePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("AN EXCEPTION OCCURED : {0}", e.ToString());
            }
            Console.WriteLine("\n");
        }
    }
}
