﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebExperience.Test.Models
{
    public class Asset 
    {
        public Asset(string prmAssetId, string prmFileName, string prmMimeType, string prmCreator, string prmEmail, string prmCountry, string prmDescription)
        {
            assetId = prmAssetId;
            country = prmCountry;
            mimeType = prmMimeType;
        }

        public string assetId {get; set;}
        public string fileName { get; set; }
        public string mimeType { get; set; }
        public string creator { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string description { get; set; }
    }
}