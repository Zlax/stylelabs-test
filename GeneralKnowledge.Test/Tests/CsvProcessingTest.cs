﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    /// 

    public class Asset{
        public Asset(string prmAssetId, string prmFileName, string prmMimeType, string prmCreator, string prmEmail, string prmCountry, string prmDescription) {
            assetId = prmAssetId;
            country = prmCountry;
            mimeType = prmMimeType;
        }

        private string assetId;
        private string fileName;
        private string mimeType;
        private string creator;
        private string email;
        private string country;
        private string description;
    }

    public class CsvProcessingTest : ITest
    {
        const int columns   = 7;
        const int aidCol    = 0;
        const int fnCol     = 1;
        const int mTypeCol  = 2;
        const int creaCol   = 3;
        const int emailCol  = 4;
        const int ctryCol   = 5;
        const int descCol   = 6;
        private bool isAssetIdFormatOk(string assetId){
            return (Regex.IsMatch(assetId, @"[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}", RegexOptions.IgnoreCase));
        }
        /* this method checks the  */
        private int checkParameters(string assetId, string	fileName, string mimeType, string createdBy, string	email, string country, string description)
        {
            /* assetId format is HEX(8)-HEX(4)-HEX(4)-HEX(4)-HEX(12) */
            if (!isAssetIdFormatOk(assetId)) return -1;
            // TODO : check the other fields
            return 0;
        }
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted
            List <int> errorLines = new List<int>();
            List <Asset> allAssets = new List<Asset>();
            try
            {
                String[] allRecords = Resources.AssetImport.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                /* the loop starts at 1 because 0 is the header line */
                for(int iRec = 1; iRec < allRecords.Length; iRec++){
                    string[] crntRecord = allRecords[iRec].Split(',');
                    if (crntRecord.Length < columns) // if not enough columns to match an asset
                    {
                        // must be an error, continue
                        errorLines.Add(iRec);
                        continue;
                    }
                    switch(checkParameters(crntRecord[aidCol], crntRecord[fnCol], crntRecord[mTypeCol], crntRecord[creaCol], crntRecord[emailCol], crntRecord[ctryCol], crntRecord[descCol]))
                    {
                        case (0): // everything is OK
                            allAssets.Add(new Asset(crntRecord[aidCol], crntRecord[fnCol], crntRecord[mTypeCol], crntRecord[creaCol], crntRecord[emailCol], crntRecord[ctryCol], crntRecord[descCol]));
                            break;
                        default:
                            errorLines.Add(iRec);
                            // TODO behaviour for other possible errors.
                            break;
                    } 
                }
            }
            catch (Exception e) {
                Console.WriteLine("AN EXCEPTION OCCURED : {0}", e.ToString());
            }
            StringBuilder builder = new StringBuilder();
            foreach (int value in errorLines)
            {
                builder.Append(value);
                builder.Append(", ");
            }
            Console.WriteLine("RESULTS : {0} records were inserted, {1} errors occured (at lines n°{2})", allAssets.Count, errorLines.Count, builder.ToString());
        }
    }
}
