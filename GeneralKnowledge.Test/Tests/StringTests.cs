﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };
                
            foreach (string possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
            Console.WriteLine("\n");
        }

        class CharCounter
        {
            private char value;
            private int count;
            public CharCounter(char newChar)
            {
                value = newChar;
                count = 1;
            }
            public bool matches(char aChar)
            {
                return (this.value == aChar);
            }
            public void oneMore()
            {
                ++this.count;
            }

            public string tellCount(){
                return "the character "+this.value+" occures "+this.count+" times ";
            }

        }

        private void GetUniqueCharsAndCount()
        {
            // TODO:
            // Write an algoritm that gets the unique characters of the word below 
            // and counts the number of occurences for each character found
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";
            
            List <CharCounter> allCounters = new List<CharCounter>();
            for (int iChar = 0; iChar < word.Length; iChar++ )
            {
                bool isNew = true;
                foreach (CharCounter counter in allCounters)
                {
                    if (counter.matches(word[iChar]))
                    {
                        counter.oneMore();
                        isNew = false;
                        break;
                    }
                }
                if(isNew)
                    allCounters.Add(new CharCounter(word[iChar]));
            }
            Console.WriteLine("In the word \"{0}\", ", word);
            foreach (CharCounter counter in allCounters)
            {
                Console.WriteLine(counter.tellCount());
            }
            Console.WriteLine("\n");
        }
    }

    public static class StringExtensions
    {
        /* Insired by http://stackoverflow.com/questions/12477339/finding-anagrams-for-a-given-word */
        private static long calcWordValue(string aWord)
        {
            int[] PRIMES = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31,
            37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103,
            107, 109, 113 };
            long result = 1L;
            aWord = aWord.ToUpper();
            for (int iChar = 0; iChar < aWord.Length; iChar++)
            {
                int charVal = aWord[iChar] - 'A';
                if (charVal < 0)
                {
                    return -1;
                }
                result *= PRIMES[charVal];
            }

            return result;
        }
        public static bool IsAnagram(this string a, string b)
        {
            // TODO: 
            // Write logic to determine whether a is an anagram of b
            return (calcWordValue(a) == calcWordValue(b));
        }
    }
}
