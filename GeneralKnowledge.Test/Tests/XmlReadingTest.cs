﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test";  } }


            class MeasureData{
                private string name;
                private int occurences;
                private double sum;
                private double lowest;
                private double highest;

                public MeasureData(string dataName, double dataValue) 
                {
                    this.name = dataName;
                    this.sum = dataValue;
                    this.occurences = 1;
                    this.lowest = dataValue;
                    this.highest = dataValue;
                }

                public bool matches(string dataName)
                {
                    return (this.name == dataName);
                }

                public void concatData(double dataValue){
                    this.sum += dataValue;
                    ++this.occurences;

                    if (dataValue < this.lowest)
                    {
                        this.lowest = dataValue;
                    }
                    else if (dataValue > this.highest)
                    {
                        this.highest = dataValue;
                    }
                }

                private string formatToLen(string aString, int aLength){
                    string toReturn;
                    int test = aLength - aString.Length;
                    if(test < 0)
                        toReturn = " " + aString.Substring(0, aLength-1);
                    else
                    {
                        toReturn = aString;
                        while (test > 0)
                        {
                            toReturn = " " + toReturn;
                            --test;
                        }
                    }
                    return toReturn;
                }

                public string prettyPrint()
                {
                    double avg = this.sum / this.occurences;
                    return (""+formatToLen(this.name, 13)+formatToLen(this.lowest.ToString(), 5)+formatToLen(avg.ToString(), 5)+formatToLen(this.highest.ToString(), 5));
                }

            }

        public void Run()
        {
            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z
            XDocument allData = XDocument.Parse(Resources.SamplePoints);
            List <MeasureData> concatMeasures = new List <MeasureData>();
            foreach (XElement measureData in allData.Descendants("param"))
            {
                bool isNew = true;
                string crntMsrName = measureData.Attribute("name").Value;
                foreach (MeasureData measure in concatMeasures)
                {
                    if (measure.matches(crntMsrName))
                    {
                        measure.concatData(Convert.ToDouble(measureData.Value));
                        isNew = false;
                        break;
                    }
                }
                if (isNew)
                {
                    concatMeasures.Add(new MeasureData(crntMsrName, Convert.ToDouble(measureData.Value)));
                }
            }

            PrintOverview(concatMeasures);
        }

        private void PrintOverview(List<MeasureData> concatMeasures)
        {
            Console.WriteLine("parameter      LOW  AVG  MAX");
            foreach(MeasureData aMeasure in concatMeasures ){
                Console.WriteLine(aMeasure.prettyPrint());
            }
            Console.WriteLine("\n");
        }
    }
}
